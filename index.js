const checkbox= document.getElementById("toggleSwitch");

checkbox.addEventListener("change", function(){
    let pricing = document.getElementsByClassName("price");
    if(checkbox.checked){
        pricing[0].innerHTML = "$19.99";
        pricing[1].innerHTML = "$24.99";
        pricing[2].innerHTML = "$39.99";
    }
    else{
        pricing[0].innerHTML = "$199.99";
        pricing[1].innerHTML = "$249.99";
        pricing[2].innerHTML = "$399.99";
    }
});